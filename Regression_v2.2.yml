Regression:
  list_description: Single-Point Regression
  image_color: orange
  dialog_group: CaseControl
  description: |
    <div style="margin:10px;"><font face="Arial, sans-serif" size = "4">
    <font size="4.5">Version 2.2 ; 2016-10-13</font></p>
    <font size="4.5">This report performs logistic or linear regression analysis on samples with variant and phenotype data. You should choose linear or logistic regression, depending on whether the outcome variable is continuous (e.g. glucose, IQ) or binary (e.g., case/control), respectively.</font></p>
    <font size="4.5">The output indicates beta-coefficients for each covariate present in the input table, including the presence of the genetic marker. The regression model constructed at each variant location has p-values, standard errors and effect sizes for the beta-coefficients.</font></p>
    <br>
    <a style="color:#0b68b2;"><b>Required parameters:</b></a><br><br>
    <table style="border-spacing:10"><tr><td><span style="color:#00aeef">genome_range </span> </td><td> all/specific region (region can be selected from an open genome browser window, e.g.chr1:36230000-36240000) </td></tr>
    <tr><td><span style="color:#00aeef">phenogrid</span> </td><td> A phenotype grid must be constructed that has columns Chrom, Pos, Reference, Call, PN, and the outcome parameter, followed by phenotype values. All phenotype and outcome values must be converted to numeric values (e.g., male=1, female=0). You can do this conveniently by filtering a phenotype table to display only the desired columns, and dragging the column headers to order them as required. </td></tr>
    <tr><td><span style="color:#00aeef">variantScope</span> </td><td> exonic only/whole genome </td></tr>
    <tr><td><span style="color:#00aeef">Model</span> </td><td> <b>autosomal dominant </b> means that any genetic marker that has just one copy will be interpreted as that marker having a value of 1 in the regression model. <b>autosomal recessive</b> means that any genetic marker must have a copy number of 2 for this marker to have a value of 1 in the regression model. </td></tr>
    <tr><td><span style="color:#00aeef">Regression</span> </td><td> logistic/linear; if the outcome measure is binary, like 'case' or 'control', then 'logistic' should be chosen. If the outcome is continuous, like IQ score or LDL cholesterol levels, then 'linear' should be chosen </td></tr>
    <tr><td><span style="color:#00aeef">Comparator</span></td><td> less than or equal to (&lt;=) OR greater than or equal to (&gt;=) the allele frequency threshold (Afthreshold)</td></tr>
    <tr><td><span style="color:#00aeef">Afthreshold</span></td><td>allele frequency threshold based on maxAf from 1000GP3,EVS, EXAC, Kyoto, GONL </td></tr>
    <tr><td><span style="color:#00aeef">VEP consequence </span></td><td>choose which variant classes to include (high, moderate, low and/or lowest impact on the gene product)</td></tr>
    <tr><td><span style="color:#00aeef">mindepth</span></td><td>1-50 [default=8]; the minimum number of reads required for a variant call</td></tr>
    </table>
    <a style="color:#0b68b2;"><b>Output Descriptions</b></a><br><br>
    <table style="border-spacing:10"><tr><td><span style="color:#00aeef">Chrom </span> </td><td> The chromosome of the variant represented as chr1, chr2, ..., chr22, chrXY, chrX, chrY, chrM.</td></tr>
    <tr><td><span style="color:#00aeef">Pos </span> </td><td>The (first) basepair position of the sequence variant</td></tr>
    <tr><td><span style="color:#00aeef">Ref </span> </td><td>The (first) Reference allele in vcf format</td></tr>
    <tr><td><span style="color:#00aeef">Call </span> </td><td>The actual called sequence (variant)</td></tr>
    <tr><td><span style="color:#00aeef">GOF </span> </td><td>Goodness of fit. If logistic regression, McFadden R^2; if linear regression, standard R^2</td></tr>
    <tr><td><span style="color:#00aeef">name </span> </td><td>Name of the coefficient. (Here, <b>Intercept</b> is the expected mean value of outcome variable Y when all predictors X=0. It captures all other factors which influence Y other than X)</td></tr>
    <tr><td><span style="color:#00aeef">coeff </span> </td><td>Coefficient value </td></tr>
    <tr><td><span style="color:#00aeef">stdError </span> </td><td> Standard error to measure the precision of the estimate of the coefficient. (The smaller the standard error, the more precise the estimate)</td></tr>
    <tr><td><span style="color:#00aeef">stdScore </span> </td><td>Standard score; coefficient normalized by StdError.</td></tr>
    <tr><td><span style="color:#00aeef">pValue</span> </td><td>Uncorrected p-value</td></tr>
    <tr><td><span style="color:#00aeef">effectSize</span> </td><td>Effect size. If logistic regression, exponential of the coefficient value; if linear regression, coefficient value.</td></tr>
    <tr><td><span style="color:#00aeef">alpha</span> </td><td>Suggested significance threshold for the uncorrected p-value, based on 0.05 divided by the number of tests performed (Bonferroni correction)</td></tr>
    </table>
    
    </font></div>

  query: |

    <#setting locale="en_US">

    <#if genome_range.val == 'All'>
    <#assign gorrange = "pgor ">
    <#assign gorrange2 = "pgor -split 200 ">
    <#else>
    <#assign gorrange = "gor -p ${genome_range.val} ">
    <#assign gorrange2 = "gor -p ${genome_range.val} ">
    </#if>

    <#assign goodcov = "source/cov/goodcov_4.wes.gord" >
    <#if 5.0 <= minReadDepth.val?number > <#assign goodcov = "source/cov/goodcov_6.wes.gord" > </#if>
    <#if 7.0 <= minReadDepth.val?number > <#assign goodcov = "source/cov/goodcov_8.wes.gord" > </#if>
    <#if 9.0 <= minReadDepth.val?number > <#assign goodcov = "source/cov/goodcov_10.wes.gord"> </#if>

    <#macro ModelFilter>
    <#if Model.val == "Additive"> </#if>
    <#if Model.val == "Autosomal dom"> | where callcopies >= 1 </#if>
    <#if Model.val == "Autosomal rec"> | where callcopies = 2 </#if>
    </#macro>

    <#macro VepFilter>
    <#if VEP_consequence.val??> | where ${VEP_consequence.val} </#if>
    </#macro>

    <#if variantScope.val == "exonic only">
      <#assign gordictionaries = "#wesVars#" >
      <#assign goodcov_scope = "wes" >
      <#assign vepfile = "source/anno/vep_v3-4-2/vep_single_wes.gord " >
    <#else>
      <#assign gordictionaries = "#wgsVars#" >
      <#assign goodcov_scope = "wgs" >
      <#assign vepfile = "source/anno/vep_v3-4-2/vep_single_wgs.gord " >
    </#if>
    
    create  ##theFreqMax## = pgor ref/freq_max.gorz | select 1-4,max_af | varmerge #3 #4 | group 1 -gc #3,#4 -max -fc max_af | rename max_max_af max_af; ${br} ${br}
    <#macro FreqFilter>
    <#if Comparator.val=="<="> | where isfloat(max_Af) and float(max_Af) <= ${Afthreshold.val} 
    <#else>|where isfloat(max_Af) and float(max_Af)>=${Afthreshold.val}
    </#if>
    </#macro>
    <#macro VepFilter>
    <#if VEP_consequence.val??> | where ${VEP_consequence.val} </#if>
    </#macro>

    create #PNs# = nor [grid: '${phenogrid.val}'] | select PN ; ${br} ${br}

    create #allvars# = ${gorrange} #wesVars# -ff [#PNs#]|varjoin -r -l -e '?' ${vepfile} <@VepFilter /> |
    varjoin -r -l -e 0  [##theFreqMax##] <@FreqFilter /> <@ModelFilter /> |
    group 1 -gc #3,#4 -count | calc dummy 1 | map -c dummy -h <(nor [#PNs#] | calc dummy 1 | 
    group -gc dummy -count | rename allCount PNs) | where allcount != PNs | hide allcount-PNs;${br}${br}

    create #phenotypes# = nor [grid: '${phenogrid.val}'] | sort -c PN; ${br}${br}

    ${gorrange2} [#allvars#] | select 1-4  |  multimap -cartesian -h [#PNs#]  |
    varjoin  -r -l -e 0 -xl pn -xr pn <(gor #wesVars# -ff [#PNs#] | select 1-4,PN,callcopies  ) 
    |join -snpseg -l -r -maxseg 10000 -e "0" -xl pn -xr pn <(gor ${goodcov} -s PN -ff [#PNs#]|calc goodcov 1)
    |replace callcopies if(goodcov=1,callcopies,0) | sort 1 -c PN
    |group 1 -gc #3-#4 -sc callcopies -lis -len 100000  | calc dummy 1 | map -c dummy -h <(nor [#allvars#] | 
    group -count | calc dummy 1 | select dummy,allcount | rename allcount numMarkers) | hide dummy | 
    /mnt/csa/R/bin/Rscript user_data/regression.v5.1.R -p=[#phenotypes#] -c=Chrom,Pos,Reference,Call -reg=${Regression.val}

  arguments:
   - name: genome_range
     optional: yes
     default: "All"
     type: position_range
     quoted: false
   - name: phenogrid
     optional: yes
     type: grid
     required_columns: ["PN"]
   - name: variantScope
     optional: yes
     type: string
     values: ["exonic only","whole genome"]
     default: "exonic only"
   - name: Model
     optional: yes
     type: string
     default: "Additive"
     values: ["Additive","Autosomal rec","Autosomal dom"]
   - name: Regression
     optional: yes
     type: string
     default: "logistic"
     values: ["logistic","linear"]
   - name: Comparator
     optional: yes
     type: string
     values: ["<=",">="]
     default: "<="
   - name: Afthreshold
     type: string
     optional: yes
     default: "0.05"
   - name: VEP_consequence
     type: string
     optional: yes
     default: ""
     values_path: VEP_impact
     format:
       keywords: contains(max_consequence,%s)
       values: max_consequence in (%s)
       empty: 2=2
   - name: minReadDepth
     advanced: yes
     optional: yes
     type: slider
     range: 1..50
     default: 8

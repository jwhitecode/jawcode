#!/usr/bin/bash
##
##	Author: Joe White
##	2016-10-17
##	Purpose: Iteratively execute a command with arguments
#
if [ -z "$1" ] && [ -z "$2" ]; then
	# $1 and $2 are required
	echo "Usage: <iterations> <delay> <command string>"
	exit
fi
iterations=$1
delay=$2
shift 2
echo $@
$@
last=0
# iteratively execute "command string" ($@) until iterations = 0.  .
until [ $last -gt 0 ]; do
	sleep $delay
	$@
	iterations=$(($iterations-1))
	if [ $iterations -eq 0 ]; then
		last=1
	fi
	echo $iterations
done
echo done


##	Author: Joe White
##	2016-10-17
##	Purpose: specifically calls monitor_mem.sh to view memory usage
#
if [ -z "$1" ] || [ -z "$2" ]; then
	echo "Usage: sh myMem.sh <iterations> <delay> [output file]"
	exit
fi

if [ -z "$3" ]; then
	sh monitor_mem.sh $1 $2 free -m | grep Mem
else
	# stdbuf temporarily sets STDOUT to unbuffered output
	echo $3
	sh monitor_mem.sh $1 $2 free -m | stdbuf -o0 grep Mem > $3
fi

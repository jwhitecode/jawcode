# For STDOUT redirection
echo "$0 $@"

# Resource files
wesVarcalls=/mnt/csa/env/test/orgs/testorg2/projects/testprj2/source/var/wes_varcalls.gord
vep_wes=/mnt/csa/env/test/orgs/testorg2/projects/testprj2/source/anno/vep_v3-4-2/vep_single_wes.gord
freqMax=/mnt/csa/env/test/orgs/testorg2/projects/testprj2/ref/freq_max.gorz
coverage=/mnt/csa/env/test/orgs/testorg2/projects/testprj2/source/cov/goodcov_8.wes.gord
rscript=/mnt/csa/R/bin/Rscript
reg_script=/mnt/csa/volumes/shared01/env/test/orgs/testorg2/rawdata/projects/testprj2/user_data/regression.v5.1.R
tophits=""
#tophits="| top 20 "
mkdir gorcache

#PN list generation:
phenodata=$1
region=$2
echo "$0 $phenodata $region"

if [ -z "$phenodata" ]; then
	echo "Usage: sh $0 <PN/Phenotype list> <genomic region/chromosome/all>"
	exit
fi
if [ -z "$region" ]; then
	echo "No region supplied; will evaluate whole genome"
	region=""
else
	region1=" -p $region "
	region=$region1
fi

# Patient Number list, pn_list
gorpipe "nor -h ./$phenodata | select pn" > ./pn.tsv

#PHENOTYPE list generations:
gorpipe "nor -h ./$phenodata | sort -c PN" > ./pheno.tsv 

if [ ! -e ./pn.tsv ] && [ ! -e ./pheno.tsv ]; then
	echo "Failed to create PN and phenotype lists."
	exit
fi

#FREQ_MAX generation:
# if freqMax.dat exists, don't recreate it, unless $3 is set.  
if [ ! -e "freqMax.tsv" ]  && [ -z "$3" ]; then
	gorpipe "pgor $freqMax | select 1-4,max_af |varmerge #3 #4 | group 1 -gc #3,#4 -max -fc max_af | rename max_max_af max_af" > ./freqMax.tsv
fi
# error out if the process failed
if [ ! -e ./freqMax.tsv ]; then
	echo "Failed to create freqMax list."
	exit
else 
	echo "freqMax.tsv exists locally"
fi

echo $region
#VARCALLS generation of ALLVARS:
# if region is set, use it, otherwise give 'em the full monty.
if [ -n "$region" ]; then
	gorpipe "create #tmp# = gor $region  $wesVarcalls PN -ff ./pn.tsv $tophits | varjoin -r -l -e '?' $vep_wes | varjoin -r -l -e 0 ./freqMax.tsv | where isfloat(max_Af) and float(max_Af) <= 0.05 | group 1 -gc #3,#4 -count | calc dummy 1 | map -c dummy -h <(nor ./pn.tsv | calc dummy 1 | group -gc dummy -count | rename allCount PNs)| where allCount != PNs | hide allCount-PNs; gor [#tmp#] | write ./allVars.gor" -workers 10 -cachedir gorcache
else
	gorpipe "create #tmp# = gor $wesVarcalls -s PN -ff ./pn.tsv $tophits | varjoin -r -l -e '?' $vep_wes | varjoin -r -l -e 0 ./freqMax.tsv | where isfloat(max_Af) and float(max_Af) <= 0.05 | group 1 -gc #3,#4 -count | calc dummy 1 | map -c dummy -h <(nor ./pn.tsv | calc dummy 1 | group -gc dummy -count | rename allCount PNs)| where allCount != PNs | hide allCount-PNs; gor [#tmp#] | write ./allVars.gor" -workers 10 -cachedir gorcache 
fi

# error out if the query failed
if [ ! -e ./allVars.gor ]; then
	echo "Failed to create allVars.gor list."
	exit
fi

if [ -z "$region" ]; then
	region1="-split 200"
	region=$region1
fi

#ALLVARS and Regression script, CURRENT:
gorpipe "pgor -split 200 ./allVars.gor | verifyorder | select 1-4 | multimap -cartesian -h <(nor ./pn.tsv) | varjoin -r -l -e 0 -xl pn -xr pn <(gor $wesVarcalls -s PN -ff ./pn.tsv | select 1-4,PN,callcopies ) |join -snpseg -l -r -maxseg 10000 -e "0" -xl pn -xr pn <(gor $coverage -s PN -ff pn.tsv | calc goodcov 1) |replace callcopies if(goodcov=1,callcopies,0 )| sort 1 -c PN |group 1 -gc #3-#4 -sc callcopies -lis -len 100000 | calc dummy 1 | map -c dummy -h <(nor ./allVars.gor | group -count | calc dummy 1 | select dummy,allcount | rename allcount numMarkers) | hide dummy | cmd $rscript $reg_script -p=./pheno.tsv -c=Chrom,Pos,Reference,Call -reg=logistic "

rm -rf gorcache
echo "Finished"

# END

